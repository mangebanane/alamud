from .action import Action3
from mud.events import ThrowInEvent

class ThrowInAction(Action3):
	EVENT = ThrowInEvent
	RESOLVE_OBJECT = "resolve_for_use"
	RESOLVE_OBJECT2 = "resolve_for_operate"
	ACTION = "throw-in"
